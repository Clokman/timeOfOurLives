import * as _ from "https://cdn.skypack.dev/lodash@4.17.21";

export function drawDays( {currentAge, maxAge=100, containerSelector= "#container-1"}) {

    const daysLived = _.range( 0, 364 * currentAge )
    const daysToGo = _.range( 0, 364 * ( maxAge - currentAge ) )


    const container = document.querySelector(containerSelector)
    container.innerHTML = ''

    daysLived.forEach( i => {
        const newDiv = document.createElement( 'div' )
        newDiv.setAttribute( 'class', 'lived' )

        container.appendChild( newDiv )
    } )


    daysToGo.forEach( i => {
        const newDiv = document.createElement( 'div' )
        newDiv.setAttribute( 'class', 'toGo' )

        container.appendChild( newDiv )
    } )
}
// drawDays({currentAge: 27, containerSelector: '#container-1'} )

// drawDays({currentAge: 35, containerSelector: '#container-2'} )
